# README #

Expands more soil types to count as clay and sand. A little cheat-y and for people who hate misreading embark site notes in regards to what's available on a given site. PRs welcome, if this breaks.

### Compatibility ###

* DF 47.x via PyLNP

### Version History

* v2 -- 47.x compat
* v1 -- 44.x compat